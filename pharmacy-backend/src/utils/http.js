const HttpStatus = {
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    OK: 200,
    CREATED: 201,
    INTERNAL_SERVER_ERROR: 500,
    NOT_FOUND: 404,
    UNPROCESSABLE_ENTITY: 422,
    PRECONDITION_FAILED: 412
};

module.exports = {
    HttpStatus
};
